import React from "react";
import { Modal, Button } from "../../../common-ui";
import { ApiPollCreatePayload } from "../../../api-client/mocks";

import styles from "./styles.module.scss";

export type ResumeModalProps = {
  modalIsOpen: boolean;
  setModalIsOpen: (b: boolean) => void;
  setForm: (f: ApiPollCreatePayload) => void;
};

export const ResumeModal = ({
  modalIsOpen,
  setModalIsOpen,
  setForm,
}: ResumeModalProps) => {
  return (
    <Modal
      modalIsOpen={modalIsOpen}
      setModalIsOpen={setModalIsOpen}
      title="Reprendre le sondage ?"
    >
      <div className={styles.message}>
        Il semblerait que vous ayez déjà commencé la création d'un sondage,
        voulez vous la poursuivre ?
      </div>

      <Button
        onClick={() => {
          setModalIsOpen(false);
        }}
        description="Reprendre"
        className={styles.button}
      />
      <Button
        onClick={() => {
          setForm({
            title: "",
            description: "",
            questions: [
              {
                description: "",
                question_type: "single_choice",
                choices: [{ description: "" }],
              },
            ],
          });
          setModalIsOpen(false);
        }}
        description="Recommencer"
        className={styles.button}
        invert
      />
    </Modal>
  );
};
