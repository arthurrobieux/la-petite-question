import React, { useEffect, useState } from "react";
import { RouteComponentProps, NavLink } from "react-router-dom";
import classnames from "classnames";

import styles from "./styles.module.scss";
import {
  Button,
  BarChart,
  DonutChart,
  backgroundColors,
} from "../../../common-ui";
import { ApiPoll } from "../../../api-client/mocks";

import logoSportEasy from "../../../../assets/img/logoSportEasy.svg";

import { PollLayout } from "../PollLayout";
import { apiClient } from "../../../api-client";

export const PollResults = ({ match }: RouteComponentProps<{ id: string }>) => {
  const [pollData, setPollData] = useState(null as ApiPoll | null);
  const [selectedView, setSelectedView] = useState("global");
  const [answersIndex, setAnswersIndex] = useState([] as number[]);

  useEffect(() => {
    apiClient.lpq.getPoll({ id: match.params.id }).then((response) => {
      setPollData(response);
      setAnswersIndex(
        Array.from(Array(response.questions[0].answers.length).keys())
      );
    });
  }, []);

  if (!pollData) {
    return (
      <PollLayout>
        Ce sondage n'existe pas.
        <Button description="Retour à l'accueil" to="/" />
      </PollLayout>
    );
  }

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <NavLink to="/">
          <img src={logoSportEasy} alt="logo" />
        </NavLink>
      </div>

      <div className={styles.formContainer}>
        <div className={styles.block}>
          <div className={styles.pollTitle}>{pollData.title}</div>
          <div className={styles.pollDescription}>{pollData.description}</div>
          <div className={styles.pollDescription}>
            {pollData.questions[0].answers.length} participants
          </div>
          <div className={styles.pollSeparator} />
          <div className={styles.views}>
            <div
              className={classnames(styles.view, {
                [styles.selectedView]: selectedView === "global",
              })}
              onClick={() => setSelectedView("global")}
              role="button"
              tabIndex={0}
            >
              Vue globale
            </div>
            <div
              className={classnames(styles.view, {
                [styles.selectedView]: selectedView === "detail",
              })}
              onClick={() => setSelectedView("detail")}
              role="button"
              tabIndex={0}
            >
              Vue détaillée
            </div>
          </div>
          <div className={styles.questions}>
            {selectedView === "global"
              ? pollData.questions.map((question, index) => (
                  <div className={styles.question}>
                    <div>
                      {index + 1} : {question.description}
                    </div>

                    <div>
                      {question.answers.map((answer) => (
                        <div className={styles.answerContainer}>
                          <div className={styles.answer}>
                            {question.question_type === "text" && (
                              <div className={styles.answerName}>
                                {answer.name} - {answer.text}
                              </div>
                            )}
                          </div>
                        </div>
                      ))}
                    </div>
                    <div>
                      {question.question_type === "single_choice" && (
                        <DonutChart
                          data={{
                            labels: question.choices.map((c) => c.description),
                            datasets: [
                              {
                                data: question.choices.map(
                                  (choice) =>
                                    question.answers.filter(
                                      (answer) =>
                                        answer.choices![0].choice_id ===
                                        choice.id
                                    ).length
                                ),
                                backgroundColor: backgroundColors,
                              },
                            ],
                          }}
                        />
                      )}

                      {question.question_type === "multiple_choices" && (
                        <BarChart
                          data={{
                            labels: question.choices.map((c) => c.description),
                            datasets: [
                              {
                                data: question.choices.map(
                                  (choice) =>
                                    question.answers.filter(
                                      (answer) =>
                                        answer.choices!.filter(
                                          (c) => c.choice_id === choice.id
                                        ).length
                                    ).length
                                ),
                                backgroundColor: backgroundColors,
                              },
                            ],
                          }}
                        />
                      )}
                    </div>
                  </div>
                ))
              : answersIndex.map((index) => {
                  return (
                    <div>
                      {pollData.questions.map((question, i) => (
                        <>
                          {i === 0 && (
                            <div className={styles.participantName}>
                              {question.answers[index].name}
                            </div>
                          )}
                          <div className={styles.questionDescription}>
                            {question.description}
                          </div>
                          {question.question_type === "text" ? (
                            <div className={styles.answer}>
                              {question.answers[index].text}
                            </div>
                          ) : (
                            <div className={styles.answer}>
                              {question.answers[index].choices!.map(
                                (choice) => (
                                  <div>
                                    {
                                      question.choices.find(
                                        (c) => c.id === choice.choice_id
                                      )!.description
                                    }
                                  </div>
                                )
                              )}
                            </div>
                          )}
                        </>
                      ))}
                    </div>
                  );
                })}
          </div>
          <div className={styles.button}>
            <Button
              description="Retour au sondage"
              to={`/poll/${match.params.id}/`}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
