import React from "react";
import { Bar } from "react-chartjs-2";
import { backgroundColors } from "../constants";

export type BarChartProps = {
  data: any;
  width?: number;
  height?: number;
  tooltips?: boolean;
};

export const BarChart = ({
  data,
  width = 100,
  height = 50,
  tooltips = false,
}: BarChartProps) => {
  return (
    <div>
      <Bar
        data={data}
        options={{
          maintainAspectRatio: true,
          scales: {
            xAxes: [
              {
                gridLines: {
                  drawOnChartArea: false,
                },
              },
            ],
            yAxes: [
              {
                ticks: {
                  beginAtZero: true,
                  stepSize: 1,
                },
              },
            ],
          },
        }}
        width={width}
        height={height}
        legend={false}
      />
      <div
        style={{
          display: "flex",
        }}
      >
        {data.labels.map((label: string, index: number) => (
          <div
            style={{
              backgroundColor: backgroundColors[index],
              color: "#fff",
              margin: "10px 5px",
              width: "fit-content",
              padding: "5px",
              borderRadius: "5px",
            }}
          >
            {label}
          </div>
        ))}
      </div>
    </div>
  );
};
