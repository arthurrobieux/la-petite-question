export interface IStylesScss {
  'donutChartContainer': string;
  'label': string;
}

export const locals: IStylesScss;
export default locals;
