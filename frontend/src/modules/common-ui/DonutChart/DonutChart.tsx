import React from "react";
import { Pie } from "react-chartjs-2";

import styles from "./styles.scss";
import { backgroundColors } from "../constants";

export type DonutChartProps = {
  data: any;
};

export const DonutChart = ({ data }: DonutChartProps) => {
  const options = {
    maintainAspectRatio: false,
    responsive: false,
    legend: {
      position: "left",
      labels: {
        boxWidth: 10,
      },
    },
  };
  return (
    <div className={styles.donutChartContainer}>
      <div
        style={{
          height: "200px",
          width: "200px",
          position: "relative",
          margin: "auto",
        }}
      >
        <Pie
          data={data}
          height={200}
          width={200}
          options={options}
          legend={{ display: false }}
        />
      </div>
      <div
        style={{
          display: "flex",
        }}
      >
        {data.labels.map((label: string, index: number) => (
          <div
            style={{
              backgroundColor: backgroundColors[index],
              color: "#fff",
              margin: "10px 5px",
              width: "fit-content",
              padding: "5px",
              borderRadius: "5px",
            }}
          >
            {label}
          </div>
        ))}
      </div>
    </div>
  );
};
